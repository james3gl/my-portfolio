
// sticky navbar 
const x=document.getElementsByClassName("navbar")
window.addEventListener('scroll',function(){
    if(document.body.scrollTop > 10 || document.documentElement.scrollTop > 10){
      x.style.top="0px";
    }
  else{
      x.style.top="30px";
  }
});

// ************************************************************************************* 


  // typed plugin  
$(document).ready(function() {
var typed = new Typed('.type-plugin', {
  strings: [
  "JAMES THOMAS",
  "a Web Developer"
  ],
  typeSpeed:60,
  backSpeed:60,
  loop:true,
    });

// ************************************************************************************* 


  //  owl - carousel 
  $('.owl-carousel').owlCarousel({
  loop:true,
  nav:false,
  margin:40,
  dotsEach:true,
  responsiveClass:true,
  videoHeight:150,
  responsive:{
      0:{
          items:1,
          nav:true
      },
      600:{
          items:2,
          nav:true
      },
      1000:{
          items:3,
          nav:true,
          loop:true
      }
  }
  })
});

  